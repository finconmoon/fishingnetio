﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;

public class TimeStamp : MonoBehaviour
{
    public enum TimeUnit
    {
        SECOND,
        MINUTE,
        HOUR,
        DAY
    }

    public TimeUnit unit = TimeUnit.MINUTE;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       // float resulttimestamp = GetCurrentTimeStamp() - recoretimestamp;
    }
    
    public void  Record()
    {
        PlayerPrefs.SetInt("TimeStamp", GetCurrentTimeStamp());
    }

    public int GetRecordTimeStamp()
    {
        if (PlayerPrefs.HasKey("TimeStamp") == false)
            Record();

        return PlayerPrefs.GetInt("TimeStamp");
    }
    
    public int GetRecordTimeStampUnit()
    {
        int timeStamp = GetRecordTimeStamp();
        return ExportToTimeStampUnit(timeStamp);
    }

    public int ExportToTimeStampUnit(int timeStamp)
    {
        switch (unit)
        {
            case TimeUnit.SECOND:
                return timeStamp;
            case TimeUnit.MINUTE:
                return timeStamp / 60;
            case TimeUnit.HOUR:
                return timeStamp / 60 / 60;
            case TimeUnit.DAY:
                return timeStamp / 60 / 60 / 24;
        }

        return 0;
    }

    public int GetCurrentTimeStamp()
    {
        var now = System.DateTime.Now.ToLocalTime();
        var span = (now - new System.DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
        int timestamp = (int)span.TotalSeconds;

        return timestamp;
    }

    public int GetCurrentTimeStampUnit()
    {
        return ExportToTimeStampUnit(GetCurrentTimeStamp());
    }
}
