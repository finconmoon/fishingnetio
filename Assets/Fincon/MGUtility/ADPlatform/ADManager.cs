﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
//using UnityEngine.SocialPlatforms.GameCenter;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class ADManager : MonoBehaviour , IUnityAdsListener
{
 
    [HideInInspector]
    public int rewardIndex;

    public NewAdmob admob;
    

    protected bool    isAdClosed = false;
    protected bool isRewardSuccess = false;
    protected bool isRewardFailed = false;


    public int interstitialFrequancy = 2;
    static int interstitialOrder = 0;
    // Start is called before the first frame update
    void Start()
    {
        Advertisement.AddListener(this);

#if UNITY_ANDROID
        Advertisement.Initialize("3727335", true);
#elif UNITY_IPHONE
        Advertisement.Initialize("3727334", true);
#endif

    }

 

    // Update is called once per frame
    public virtual void Update()
    {
        if (admob.isADClosed)
        {
            isAdClosed = true;
            admob.isADClosed = false;
        }

        if (admob.isRewardSuccess)
        {
            isRewardSuccess = true;
            admob.isRewardSuccess = false;
        }

        if (admob.isRewardFailed)
        {
            isRewardFailed = true;
            admob.isRewardFailed = false;
        }

        if (isAdClosed)
        {
            if (isRewardSuccess)
            {
                Time.timeScale = 1.0f;
            }
        }
                
        ProcessThisGamesReward();

        isAdClosed = false;
        isRewardSuccess = false;
        isRewardFailed = false;
    }

    public virtual  void ProcessThisGamesReward()
    {

    }

    public bool IsRewardAdEnable()
    {
        if ((Advertisement.IsReady("rewardedVideo") || admob.IsRewardVideoReady()))
            return true;

        return false;
    }

    
    public void ShowInterstitial()
    {
        if (interstitialOrder % interstitialFrequancy != 0)
            return;
        
        if (admob.IsInterstitialReady())
        {
            admob.ShowInterstitial();
        }
        else if (Advertisement.IsReady("video"))
        {
            Advertisement.Show("video");
        }

        interstitialOrder++;
    }

    public void RequestReward(int   typeIndex)
    {
        rewardIndex = typeIndex;
#if UNITY_EDITOR
        //pc test
        isAdClosed = true;
        isRewardSuccess = true;
#endif
        if (admob.IsRewardVideoReady())
        {
            admob.ShowRewardVideo();
         //   Time.timeScale = 0.0f;
        }
        else if (Advertisement.IsReady("rewardedVideo"))
        {
            Advertisement.Show("rewardedVideo");
        }
    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        if (showResult == ShowResult.Finished)
        {
            isRewardSuccess = true;
            // Reward the user for watching the ad to completion.
        }
        else if (showResult == ShowResult.Skipped)
        {
            isRewardFailed = true;
            // Do not reward the user for skipping the ad.
        }
        else if (showResult == ShowResult.Failed)
        {
            isRewardFailed = true;
            Debug.LogWarning("The ad did not finish due to an error.");
        }

        isAdClosed = true;
    }

    public void OnUnityAdsReady(string placementId)
    {
        // If the ready Placement is rewarded, show the ad:
   //     if (placementId == myPlacementId)
        {
            // Optional actions to take when the placement becomes ready(For example, enable the rewarded ads button)
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }

    // When the object that subscribes to ad events is destroyed, remove the listener:


}
