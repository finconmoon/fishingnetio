﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ADManagerForThisGame : ADManager
{
    public enum RewardType
    {
        FREECOIN = 0,

    };

    RewardType rewardType;
    public Shop         shop;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();
    }

    public override void ProcessThisGamesReward()
    {
        rewardType = (RewardType)rewardIndex;
        if (isAdClosed)
        {
            if (isRewardSuccess)
            {
                switch (rewardType)
                {
                    case RewardType.FREECOIN:
                        break;
                }
            }

            if (isRewardFailed)
            {
                switch (rewardType)
                {
                    case RewardType.FREECOIN:

                        break;
                   
                }
            }
            Time.timeScale = 1.0f;
        }
    }
}
