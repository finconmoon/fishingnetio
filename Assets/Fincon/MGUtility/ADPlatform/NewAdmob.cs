﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using UnionAssets.FLE;
//using Batch;
//using AppodealAds.Unity.Api;
//using AppodealAds.Unity.Common;
using GoogleMobileAds.Api;


public class NewAdmob : MonoBehaviour {


	BannerView bannerView;
	InterstitialAd interstitial;
	RewardBasedVideoAd rewardBasedVideo;

    [HideInInspector]
	public bool isRewardSuccess = false;
    [HideInInspector]
    public bool isRewardFailed = false;
    [HideInInspector]
    public bool isADClosed = false;

    public void Awake()
    {
		
    }

    public void Start()
    {
        #if UNITY_ANDROID
                                    string appId = "ca-app-pub-1135994090688225~2302116993";
        #elif UNITY_IPHONE
               // string appId = "ca-app-pub-1135994090688225~2554437394";
        #else
                                    string appId = "unexpected_platform";
        #endif

//        MobileAds.Initialize("ca-app-pub-1135994090688225~2302116993");
        MobileAds.Initialize(initStatus => { });
        InitAdmob();
    }

    void	OnDestroy()
	{
		if (bannerView != null)
			bannerView.Destroy();
		if (interstitial != null)
			interstitial.Destroy();
    }

	public void	InitAdmob()
	{
        RequestInterstitial();
        RequestBanner(false);
		RequestRewardBasedVideo ();
	}

	public void RequestBanner(bool isTop)
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-3940256099942544/6300978111";
#else
		string adUnitId = "ca-app-pub-3940256099942544/2934735716";
#endif

        // Create a 320x50 banner at the top of the screen.

        if (bannerView != null)
			bannerView.Destroy();
			
		if (isTop)
			bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
		else
			bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
		
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
        bannerView.Show();

	}

	public void RequestInterstitial()
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-3940256099942544/8691691433";
#else
		string adUnitId = "ca-app-pub-3940256099942544/5135589807";
#endif

        if (interstitial != null)
			interstitial.Destroy();
		
		interstitial = new InterstitialAd (adUnitId);
		interstitial.OnAdClosed += OnInterstitialClosing;

        if (IsInterstitialReady())
            return;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
		// Load the interstitial with the request.
		interstitial.LoadAd(request);
	}

	public void RequestRewardBasedVideo()
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-3940256099942544/5224354917";
#else
		string adUnitId = "ca-app-pub-3940256099942544/1712485313";
#endif

        if (rewardBasedVideo == null) {
			rewardBasedVideo = RewardBasedVideoAd.Instance;
			rewardBasedVideo.OnAdClosed += OnRewardClosing;
			rewardBasedVideo.OnAdRewarded += OnRewarded;
        }

      //  if (IsRewardVideoReady())
        //    return;

        AdRequest request = new AdRequest.Builder().Build();
		rewardBasedVideo.LoadAd(request, adUnitId);
	}

	public void OnInterstitialClosing(object sender, System.EventArgs args)
	{
        isADClosed = true;
    }

    public void OnRewardClosing(object sender, System.EventArgs args)
    {
        isADClosed = true;

        if (isRewardSuccess == false)
        {
            isRewardFailed = true;
        }

        RequestRewardBasedVideo();
//        interstitial.IsLoaded = false;
    }


    public void OnRewarded(object sender, Reward r)
	{
        isRewardSuccess = true;
        
    }

	public RewardBasedVideoAd GetRewardBasedVideoAd()
	{
		return rewardBasedVideo;
	}

	public InterstitialAd GetInterstitialAd()
	{
		return interstitial;
	}

	public	void	ShowBanner()
	{
		if (bannerView != null)
			bannerView.Show ();
	}

	public	void	HideBanner()
	{
		if (bannerView != null)
			bannerView.Hide ();
	}

	public	bool	IsInterstitialReady()
	{
		if (interstitial != null)
			return interstitial.IsLoaded ();

		return false;
	}

	public	void	ShowInterstitial()
	{
		if (interstitial != null)
			interstitial.Show ();
	}
		
	public	bool	IsRewardVideoReady()
	{
		if (rewardBasedVideo != null)
			return rewardBasedVideo.IsLoaded ();

		return false;
	}

	public	void	ShowRewardVideo()
	{
		if (rewardBasedVideo != null)
			rewardBasedVideo.Show ();


    }

	
	// Update is called once per frame
	void Update () {
		
	}

}
