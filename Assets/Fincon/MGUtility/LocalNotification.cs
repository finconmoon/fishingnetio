﻿using System.Collections;
using System.Collections.Generic;
//using UnityEngine.Timeline;
using System;
using UnityEngine;

#if UNITY_IOS
    using Unity.Notifications.iOS;
#else
    using Unity.Notifications.Android;
#endif



public class LocalNotification : MonoBehaviour
{

    public LocalNotice[] notifications;
    // Start is called before the first frame update
    void Start()
    {

#if UNITY_IOS


#else
        var c = new AndroidNotificationChannel()
        {
            Id = "Fincon",
            Name = "Default Channel",
            Importance = Importance.High,
            Description = "Generic notifications",
        };
        AndroidNotificationCenter.RegisterNotificationChannel(c);
#endif
        RemoveNotify();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnApplicationFocus(bool focusStatus)
    {
        if (focusStatus == false)
        {
            RemoveNotify();

            for (int i = 0; i < notifications.Length; i++)
            {
                AndroidNotify(notifications[i].hours, notifications[i].minutes, notifications[i].seconds, notifications[i].title, notifications[i].text);
                iOSNotify(notifications[i].hours, notifications[i].minutes, notifications[i].seconds, notifications[i].title, notifications[i].text);
            }
        }
        else
        {
            RemoveNotify();
        }
    }


    void AndroidNotify(int hours, int minutes, int seconds, string title, string message)
    {
#if UNITY_IOS

#else
        var notification = new AndroidNotification();
        notification.Title = title;
        notification.Text = message;
        notification.FireTime = System.DateTime.Now.Add(new TimeSpan(hours, minutes, seconds));

        AndroidNotificationCenter.SendNotification(notification, "Fincon");
#endif
    }

        void iOSNotify(int hours, int minutes, int seconds,string title,string message)
    {
#if UNITY_IOS
       
        var timeTrigger = new iOSNotificationTimeIntervalTrigger()
        {
            TimeInterval = new TimeSpan(hours,minutes,seconds),
            Repeats = false
        };

        var notification = new iOSNotification()
        {
            // You can optionally specify a custom identifier which can later be 
            // used to cancel the notification, if you don't set one, a unique 
            // string will be generated automatically.
            Identifier = "_notification_01",
            Title = title,
    //        Body = "Scheduled at: " + DateTime.Now.ToShortDateString() + " triggered in 5 seconds",
            Subtitle = message,
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
            CategoryIdentifier = "category_a",
            ThreadIdentifier = "thread1",
            Trigger = timeTrigger,
        };

        iOSNotificationCenter.ScheduleNotification(notification);

#endif
    }

    void RemoveNotify()
    {
#if UNITY_IOS
        iOSNotificationCenter.RemoveAllDeliveredNotifications();
        iOSNotificationCenter.RemoveAllScheduledNotifications();
#else
        AndroidNotificationCenter.CancelAllNotifications();

#endif
    }
    
}
