﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalNotice : MonoBehaviour
{
    public int hours;
    public int minutes;
    public int seconds;

    public string title;
    public string text;
}