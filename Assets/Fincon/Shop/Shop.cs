﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using UnityEngine.Purchasing;


public class Shop : MonoBehaviour, IStoreListener {

	//int	gold = 0;
	int currentGold = 0;

//	public	GameObject	carsRoot;
//	public	StageManager	stageManager;
//	public  StreamingManager	streamingManager;
//	public  UIManager	uiManager;
	
	//[HideInInspector]

	public	static	Shop shopInstance = null;

	public	bool	purchaseEnable = true;
	GameObject	currentCar = null;
	public	GameObject	loadingPanel;
	public	GameObject[]	allCarButtons;

	private static IStoreController storeController;
	private static IExtensionProvider extensionProvider;


	public const string productId1 = "td_tier1coin";
	public const string productId2 = "td_tier2coin";
	public const string productId3 = "td_tier5coin";
	public const string productId4 = "tier15coin";
	public const string productId5 = "tier20coin";
	public const string productId6 = "trafficroad_allcars";

    public bool isGameOver = false;

	void Awake () {

      //  PlayerPrefs.DeleteAll();

        if (IsCharacterUnlocked(0) != 1)
            UnlockCharacter(0);

        shopInstance = this;
		if (PlayerPrefs.HasKey ("Gold") == false)
			PlayerPrefs.SetInt("Gold",200);

        if (PlayerPrefs.HasKey("selectedCharacter") == false)
            PlayerPrefs.SetInt("selectedCharacter", 0);
        
        

        InitializePurchasing ();
		//init ();
	}

	//private static bool IsInitialized = false;
	public void init() {
		

		/*
		if(!IsInitialized) {
			//Event Use Examples
			SA.IOSNative.StoreKit.PaymentManager.OnVerificationComplete += HandleOnVerificationComplete;
			SA.IOSNative.StoreKit.PaymentManager.OnStoreKitInitComplete += OnStoreKitInitComplete;
			
			
			SA.IOSNative.StoreKit.PaymentManager.OnTransactionComplete += OnTransactionComplete;
			SA.IOSNative.StoreKit.PaymentManager.OnRestoreComplete += OnRestoreComplete;
			
			
			IsInitialized = true;
		} 
		
		SA.IOSNative.StoreKit.PaymentManager.Instance.LoadStore();*/
	}

	// Update is called once per frame
	void Update () {
	
	}

    

    public int IsCharacterNoThanks(int index)
    {
        string charname = "CharacterNoThanks" + index.ToString();
        //		 return 1;
        return PlayerPrefs.GetInt(charname);
    }

    public int IsCharacterUnlocked(int index)
	{
		string charname = "Character" + index.ToString();
//		 return 1;
		return PlayerPrefs.GetInt(charname);
	}

    public bool NoThanksCharacter(int index)
    {
        string charname = "CharacterNoThanks" + index.ToString();

        if (PlayerPrefs.GetInt(charname) == 1)
            return false;

        PlayerPrefs.SetInt(charname, 1);
        return true;
    }


    public bool UnlockCharacter(int index)
	{
		string charname = "Character" + index.ToString();

		if (PlayerPrefs.GetInt(charname) == 1)
			return false;

		PlayerPrefs.SetInt(charname, 1);
		return true;
	}

    public  int GetSelectedCharacter()
    {
        return PlayerPrefs.GetInt("selectedCharacter");
    }

	public void SelectCharacter(int index)
	{
		PlayerPrefs.SetInt("selectedCharacter", index);
	}
    
    
    public void	GainGold(int	gainGold, bool force = false)
    {
        if (isGameOver && !force)
            return;

		PlayerPrefs.SetInt("Gold",PlayerPrefs.GetInt("Gold") + gainGold);
	}

	public	void	GainCurrentGold(int	gainGold,bool force = false){
        if (isGameOver && !force)
            return;

        currentGold += gainGold;
	}

	public	int	GetGold(){
		return PlayerPrefs.GetInt("Gold");
	}

	public	void	SetGold(int	gold){

        if (isGameOver)
            return;

        PlayerPrefs.SetInt("Gold",gold);
	}

	public	int	GetCurrentAddedGold(){
		return	currentGold;
	}

	public	void	ApplyCurrentGold(){
        GainGold(currentGold,true);
		currentGold = 0;
	}

	public	bool	UseGold(int	useGold){

		if (GetGold() >= useGold) {
			SetGold (GetGold () - useGold);
			return true;
		}
		return false;
	}

   
    public void	Tier2Coin(){

		if (shopInstance.purchaseEnable == true) {
			BuyProductID("td_tier1coin");
		}
	}


	public	void	Tier4Coin(){
		if (shopInstance.purchaseEnable == true) {
			BuyProductID("td_tier2coin");
		}
	}

	public	void	Tier10Coin(){
		if (shopInstance.purchaseEnable == true) {
			BuyProductID("td_tier5coin");
		}
	}

	public	void	Tier30Coin(){
		if (shopInstance.purchaseEnable == true) {
			BuyProductID("tier15coin");
		}
	}

	public	void	Tier40Coin(){
		if (shopInstance.purchaseEnable == true) {
			BuyProductID("tier20coin");
		}
	}

	public	void	UnlockAllCars(){
		if (shopInstance.purchaseEnable == true) {
			BuyProductID("trafficroad_allcars");
		}
	}

	public	void	RestoreItems(){
		if (shopInstance.purchaseEnable == true) {
			RestorePurchase ();
		}
	}

	public	void	ActiveLoading(){
		#if UNITY_IPHONE
		Handheld.SetActivityIndicatorStyle(UnityEngine.iOS.ActivityIndicatorStyle.WhiteLarge);
		#elif UNITY_ANDROID
		Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
		#endif

	
		shopInstance.purchaseEnable = false;
		Handheld.StartActivityIndicator();
		loadingPanel.SetActive (true);

	}

	public	void	EndLoading(){

		shopInstance.purchaseEnable = true;
		Handheld.StopActivityIndicator ();
		loadingPanel.SetActive (false);
	}


	private bool IsInitialized()
	{
		return (storeController != null && extensionProvider != null);
	}

	public void InitializePurchasing()
	{
		if (IsInitialized())
			return;
        /*
#if UNITY_IOS

        var module = StandardPurchasingModule.Instance();

		ConfigurationBuilder builder = ConfigurationBuilder.Instance(module);

		builder.AddProduct(productId1, ProductType.Consumable, new IDs
			{
				{ productId1, AppleAppStore.Name },
			});

		builder.AddProduct(productId2, ProductType.Consumable, new IDs
			{
				{ productId2, AppleAppStore.Name },
			});

		builder.AddProduct(productId3, ProductType.Consumable, new IDs
			{
				{ productId3, AppleAppStore.Name },
			});

		builder.AddProduct(productId4, ProductType.Consumable, new IDs
			{
				{ productId4, AppleAppStore.Name },
			});

		builder.AddProduct(productId5, ProductType.Consumable, new IDs
			{
				{ productId5, AppleAppStore.Name },
			});
				
		builder.AddProduct(productId6, ProductType.Consumable, new IDs
			{
				{ productId6, AppleAppStore.Name },
			});
				
		UnityPurchasing.Initialize(this, builder);

#endif*/

	}

	public void BuyProductID(string productId)
	{
		try
		{
			if (IsInitialized())
			{
				Product p = storeController.products.WithID(productId);

				if (p != null && p.availableToPurchase)
				{
					ActiveLoading();
					Debug.Log(string.Format("Purchasing product asychronously: '{0}'", p.definition.id));
					storeController.InitiatePurchase(p);
				}
				else
				{
					Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
				}
			}
			else
			{
				Debug.Log("BuyProductID FAIL. Not initialized.");
			}
		}
		catch (System.Exception e)
		{
			Debug.Log("BuyProductID: FAIL. Exception during purchase. " + e);
		}
	}

	public void RestorePurchase()
	{
		ActiveLoading();

		if (!IsInitialized())
		{
			Debug.Log("RestorePurchases FAIL. Not initialized.");
			return;
		}

		if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer)
		{
			Debug.Log("RestorePurchases started ...");
            /*
#if UNITY_IOS
            var apple = extensionProvider.GetExtension<IAppleExtensions>();

			apple.RestoreTransactions
			(
				(result) => { Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore."); }
			);
#endif*/
        }
		else
		{
			Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
	}

	public void OnInitialized(IStoreController sc, IExtensionProvider ep)
	{
		Debug.Log("OnInitialized : PASS");

		storeController = sc;
		extensionProvider = ep;
	}

	public void OnInitializeFailed(InitializationFailureReason reason)
	{
		Debug.Log("OnInitializeFailed InitializationFailureReason:" + reason);
	}

	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
	{
		Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

		UnlockProducts(args.purchasedProduct.definition.id);
		EndLoading();

		return PurchaseProcessingResult.Complete;
	}

	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		EndLoading();
		Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
	}

    private static void UnlockProducts(string productIdentifier) {
		switch(productIdentifier) {
		case productId1:
			shopInstance.GainGold (1500);
			shopInstance.GetComponent<AudioSource>().Play();
			break;
		case productId2:
			shopInstance.GainGold (3000);
			shopInstance.GetComponent<AudioSource>().Play();
			break;
		case productId3:
			shopInstance.GainGold (7500);
			shopInstance.GetComponent<AudioSource>().Play();
			break;
		case productId5:
			shopInstance.GainGold (33000);
			shopInstance.GetComponent<AudioSource>().Play();
			break;
		case productId4:
			shopInstance.GainGold (25000);
			shopInstance.GetComponent<AudioSource>().Play();
			break;
		case "trafficroad_allcars":
			PlayerPrefs.SetInt ("unlockAllCars", 1);
	//		shopInstance.unlockAllCars = true;
			shopInstance.GetComponent<AudioSource> ().Play ();
			for (int i = 0;i < 3;i++)
				shopInstance.allCarButtons[i].SetActive (false);
			break;

		}
	}


	/*
	private static void OnTransactionComplete (SA.IOSNative.StoreKit.PurchaseResult result) {
		
		Debug.Log("OnTransactionComplete: " + result.ProductIdentifier);
		Debug.Log("OnTransactionComplete: state: " + result.State);
		
		switch(result.State) {
		case SA.IOSNative.StoreKit.PurchaseState.Restored:
			//Our product been succsesly purchased or restored
			//So we need to provide content to our user depends on productIdentifier
			UnlockProducts(result.ProductIdentifier);
			break;
		case SA.IOSNative.StoreKit.PurchaseState.Deferred:
			//iOS 8 introduces Ask to Buy, which lets parents approve any purchases initiated by children
			//You should update your UI to reflect this deferred state, and expect another Transaction Complete  to be called again with a new transaction state 
			//reflecting the parent’s decision or after the transaction times out. Avoid blocking your UI or gameplay while waiting for the transaction to be updated.
			break;
		case SA.IOSNative.StoreKit.PurchaseState.Failed:
			//Our purchase flow is failed.
			//We can unlock intrefase and repor user that the purchase is failed. 
			Debug.Log("Transaction failed with error, code: " + result.Error.Code);
			//Debug.Log("Transaction failed with error, description: " + result.Error.Message.);
			
			
			break;
		}

		shopInstance.EndLoading();

		if(result.State == SA.IOSNative.StoreKit.PurchaseState.Failed) {
			//IOSNativePopUpManager.showMessage("Transaction Failed", "Error code: " + result.Error.Code + "\n" + "Error description:" + result.Error.Description);
		} else {
		//	IOSNativePopUpManager.showMessage("Store Kit Response", "product " + result.ProductIdentifier + " state: " + result.State.ToString());
		}
		
	}
	

	private static void OnRestoreComplete (SA.IOSNative.StoreKit.RestoreResult res) {
		if(res.IsSucceeded) {
			IOSNativePopUpManager.showMessage("Success", "Restore Compleated");
		} else {
			IOSNativePopUpManager.showMessage("Error: " + res.Error.Code, res.Error.Message);
		}
		shopInstance.EndLoading();
	}	
	
	
	static void HandleOnVerificationComplete (SA.IOSNative.StoreKit.VerificationResponse response) {
	//	IOSNativePopUpManager.showMessage("Verification", "Transaction verification status: " + response.status.ToString());
		
		Debug.Log("ORIGINAL JSON: " + response.OriginalJSON);
	}
	

	private static void OnStoreKitInitComplete(SA.Common.Models.Result result) {
		
		if(result.IsSucceeded) {
			
			int avaliableProductsCount = 0;

			foreach(SA.IOSNative.StoreKit.Product tpl in SA.IOSNative.StoreKit.PaymentManager.Instance.Products) {
				if(tpl.IsAvailable) {
					avaliableProductsCount++;
				}
			}
			
		//	IOSNativePopUpManager.showMessage("StoreKit Init Succeeded", "Available products count: " + avaliableProductsCount);
			Debug.Log("StoreKit Init Succeeded Available products count: " + avaliableProductsCount);
		} else {
		//	IOSNativePopUpManager.showMessage("StoreKit Init Failed",  "Error code: " + result.Error.Code + "\n" + "Error description:" + result.Error.Description);
		}
	}
*/
}
