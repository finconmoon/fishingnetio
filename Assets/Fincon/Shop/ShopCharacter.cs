﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShopCharacter : MonoBehaviour
{
    public TextMeshPro teamName;

    float animTime = 0.0f;
    float currentTime = 0.0f;

    bool isFocus = false;

    public Material baseMat;
    // Start is called before the first frame update
    void Start()
    {
        animTime = Random.Range(3.0f, 5.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (animTime < currentTime && isFocus)
        {
            PlayRandomAnim();
        }

        currentTime += Time.deltaTime;
    }

    void    PlayRandomAnim()
    {
        switch (Random.Range(0, 5))
        {
            case 0:
                GetComponentInChildren<Animator>().Play("pc01_LobbyIdle01");
                break;
            case 1:
                GetComponentInChildren<Animator>().Play("pc01_LobbyIdle02");
                break;
            case 2:
                GetComponentInChildren<Animator>().Play("pc01_LobbyIdle03");
                break;
            case 3:
                GetComponentInChildren<Animator>().Play("pc01_LobbyIdle04");
                break;
            case 4:
                GetComponentInChildren<Animator>().Play("pc01_LobbyIdle06");
                break;
        }
        
        animTime = Random.Range(2.0f, 4.0f) + currentTime;
    }
    public void Focused(bool _isFocus)
    {
        isFocus = _isFocus;

        if (isFocus)
        {
            PlayRandomAnim();
        }
        else
        {

        }
    }

    public void UpdateCharacter(int matIndex)
    {
        
    }

    public void UpdateBlackCharacter()
    {
      /*  SkinnedMeshRenderer[] skins = GetComponentsInChildren<SkinnedMeshRenderer>();

        for (int i = 0; i < skins.Length; i++)
        {
            skins[i].material = baseMat;
            teamName.text = "???";
        }*/
    }
}
