﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class  HockeyPlayer : MonoBehaviour
{
    Vector3 mousePos;

    public float moveLRDist = 3.0f;
    public float mouseDeltaLRMax = 1.0f;
    public float steeringLRSpeed = 3.0f;
    public float forwardSpeed = 100.0f;
    public float maxRotAngle = 45.0f;
    public float noInputForwardTime = 0.5f;
    public float driftGrip;
    public float speedUpRatio = 1.0f;
    public float speedUpRatioMax = 1.0f;
    public float wallXDist = 2.0f;

   // int currentBodyCheckCount = 0;
    [HideInInspector]
    public int bodyCheckCount = 0;
    public int bodyCheckCountMax = 10;
    
    public GameObject puckMode;

    public GameObject readyCamera;
    public GameObject hockeyCamera;
    public GameObject puckCamera;
    public GameObject flyCamera;

    public Animator   animator;
    public GameObject fakePuck;


    public GameObject dashEffect;
    public int bodyCheckScore = 0;
    
    [HideInInspector]
    public int bodyCheckGold = 0;
    Shop shop;
    
    // Start is called before the first frame update
    void Start()
    {
        mousePos = Input.mousePosition;
        AwakeFSM();

        puckMode.SetActive(false);
        bodyCheckCount = 0;
        shop = FindObjectOfType<Shop>();

        ChangeCharacter(shop.GetSelectedCharacter());
    }


    public  void    ChangeCharacter(int index)
    {
        shop.SelectCharacter(index);
    }

   public void DashRun(bool enableInput)
    {
        if (Input.GetMouseButtonDown(0))
            mousePos = Input.mousePosition;

        float deltaX = (Input.mousePosition.x - mousePos.x) / (float)Screen.width;
          if (!enableInput)
            deltaX = 0.0f;

        if (deltaX > mouseDeltaLRMax)
            deltaX = mouseDeltaLRMax;

        if (deltaX < -mouseDeltaLRMax)
            deltaX = -mouseDeltaLRMax;

        deltaX *= 1000.0f;

        float deltaTime = 0.02f;
        deltaTime = Time.deltaTime;

        float inputX = deltaX;
        mousePos = Input.mousePosition;

        
    }
    

    public void Update()
    {
        UpdateFSM();
}

    public void SaveMe()
    {
        if (deadState == State.Run)
        {
            SetState(State.Run);
        }

        Time.timeScale = 1.0f;
    }

    public void OnCollisionEnter(Collision collision)
    {
      
    }
    

    public void OnTriggerEnter(Collider other)
    {
       
    }

}
