﻿using UnityEngine;
using System.Collections;

public partial class HockeyPlayer : MonoBehaviour {

	public enum State
	{
		Ready,
		Run,
        GameOver,
	}

	public State currentState;
    public State deadState;
    protected bool	_isNewState = false;
	protected float stateTime;

    public bool isFinish = false;
	// Use this for initialization
	void AwakeFSM () {
		SetState(State.Ready);
		StartCoroutine("FSMMain");
	}
	
	void	UpdateFSM()
	{
		stateTime += Time.deltaTime;
	}

	public	void SetState(State newState)
	{
		_isNewState = true;
		currentState = newState;
		stateTime = 0.0f;
	}
	
	IEnumerator FSMMain()
	{
		while(Application.isPlaying)
		{
			_isNewState = false;
			yield return StartCoroutine("On" + currentState.ToString());
		}
	}


    float animTime = 0.0f;
	IEnumerator OnReady()
	{
        readyCamera.SetActive(true);
        hockeyCamera.SetActive(false);
        
        //
        do
		{
			yield return null;
			if(_isNewState) break;
            
			//
		} while(!_isNewState);
	}

	IEnumerator OnRun()
	{
        mousePos = Input.mousePosition;

        readyCamera.SetActive(false);
        hockeyCamera.SetActive(true);
        
        transform.forward = new Vector3(0, 0, 1);
        
        //
        do
		{
            //
            yield return null;
			if(_isNewState) break;

            DashRun(true);
            
        } while(!_isNewState);
	}

    IEnumerator OnDash()
    {
         GetComponent<Rigidbody>().transform.forward = new Vector3(0, 0, 1);
        dashEffect.SetActive(true);

        speedUpRatio *= 1.5f;
        
        do
        {
            yield return null;
            if (_isNewState) break;
            

        } while (!_isNewState);
    }
    
    IEnumerator OnGameOver()
    {
        GetComponent<Rigidbody>().transform.forward = new Vector3(0, 0, 1);
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
        
        //
        do
        {
            yield return null;
            if (_isNewState) break;

            //
        } while (!_isNewState);
    }


}
