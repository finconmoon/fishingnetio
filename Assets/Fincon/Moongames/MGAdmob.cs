﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using UnionAssets.FLE;
//using Batch;
//using AppodealAds.Unity.Api;
//using AppodealAds.Unity.Common;
using GoogleMobileAds.Api;


public class MGAdmob : MonoBehaviour {

	BannerView bannerView;
	InterstitialAd interstitial;
	RewardBasedVideoAd rewardBasedVideo;

	static MGAdmob	admob = null;

	bool	successReward = false;
	public static MGAdmob instance {
		get {
			if(admob ==  null) {
				GameObject go =  new GameObject("MGAdmob");
				DontDestroyOnLoad(go);
				admob =  go.AddComponent<MGAdmob>();

				admob.InitAdmob ();
			}

			return admob;
		}
	}

	void	OnDestroy()
	{
		if (bannerView != null)
			bannerView.Destroy();
		if (interstitial != null)
			interstitial.Destroy();
	}

	public void	InitAdmob()
	{
		RequestBanner(false);
        RequestInterstitial();
		RequestRewardBasedVideo ();
	}

	public void RequestBanner(bool isTop)
	{
		#if UNITY_ANDROID
		string adUnitId = "INSERT_ANDROID_BANNER_AD_UNIT_ID_HERE";
		#else
        string adUnitId = "ca-app-pub-1135994090688225/6030390140";
		#endif

		// Create a 320x50 banner at the top of the screen.

		if (bannerView != null)
			bannerView.Destroy();
			
		if (isTop)
			bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Top);
		else
			bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
		
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
		bannerView.Hide ();

	}

	public void RequestInterstitial()
	{
		#if UNITY_ANDROID
		string adUnitId = "INSERT_ANDROID_INTERSTITIAL_AD_UNIT_ID_HERE";
		#else
        string adUnitId = "ca-app-pub-1135994090688225/8286562981";
		#endif

		if (interstitial != null)
			interstitial.Destroy();
		
		interstitial = new InterstitialAd (adUnitId);
		interstitial.OnAdClosed += OnInterstitialClosing;

		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the interstitial with the request.
		interstitial.LoadAd(request);
	}

	public void RequestRewardBasedVideo()
	{
		#if UNITY_ANDROID
		string adUnitId = "ca-app-pub-3940256099942544/5224354917";
		#else
        string adUnitId = "ca-app-pub-1135994090688225/3212655111";
		#endif

		if (rewardBasedVideo == null) {
			rewardBasedVideo = RewardBasedVideoAd.Instance;
			rewardBasedVideo.OnAdClosed += OnRewardClosing;
			rewardBasedVideo.OnAdRewarded += OnRewarded;
		}
			
		AdRequest request = new AdRequest.Builder().Build();
		rewardBasedVideo.LoadAd(request, adUnitId);
	}

	public void OnInterstitialClosing(object sender, System.EventArgs args)
	{
		RequestInterstitial();
	}

	public void OnRewardClosing(object sender, System.EventArgs args)
	{
		RequestRewardBasedVideo();

		if (successReward == false)
			Time.timeScale = 1.0f;
	}

	public void OnRewarded(object sender, Reward r)
	{
		successReward = true;
	}

	public bool isSuccessReward()
	{
		return successReward;
	}


	public RewardBasedVideoAd GetRewardBasedVideoAd()
	{
		return rewardBasedVideo;
	}

	public InterstitialAd GetInterstitialAd()
	{
		return interstitial;
	}

	public	void	ShowBanner()
	{
		if (bannerView != null)
			bannerView.Show ();
	}

	public	void	HideBanner()
	{
		if (bannerView != null)
			bannerView.Hide ();
	}

	public	bool	IsInterstitialReady()
	{
		if (interstitial != null)
			return interstitial.IsLoaded ();

		return false;
	}

	public	void	ShowInterstitial()
	{
		if (interstitial != null)
			interstitial.Show ();
	}
		
	public	bool	IsRewardVideoReady()
	{
		if (rewardBasedVideo != null)
			return rewardBasedVideo.IsLoaded ();

		return false;
	}

	public	void	ShowRewardVideo()
	{
		successReward = false;
		if (rewardBasedVideo != null)
			rewardBasedVideo.Show ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

}
