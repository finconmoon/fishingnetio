﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageController : MonoBehaviour
{
    public TubePlayer playerPrefab;
    //public ArrayList players = new ArrayList();

    [HideInInspector]
    public GameObject[] players;

    public int playerCount = 8;

    public Stage[] stages;

    [HideInInspector]
    public Stage curStage;

    public bool isDead = false;
    public int testStage = 3;
    public bool useTestStage = false;

    public GameObject coinPrefab;
    public float coinSpawnTime = 1.0f;
    //public float coinSpawnRadius = 1.0f;
    float currentCoinSpawnTime = 0.0f;
    public int coinMaxCount = 10;
    public int coinSpawnMaxCount = 100;
    int coinSpawnCount = 0;

    public GameObject coinRoot;

    public bool isStageMode = false;
    
    void Awake()
    {
        if (!PlayerPrefs.HasKey("Level"))
        {
            // PlayerPrefs.SetInt("Level", 1);
        }
    }

    void Start()
    {
       InitStage();
    }

    public void ClearLevel()
    {
        if (isStageMode)
            PlayerPrefs.SetInt("Level",PlayerPrefs.GetInt("Level") + 1);
    }

    public int GetLevel()
    {
        if (!PlayerPrefs.HasKey("Level"))
        {
            return 1;
          //  PlayerPrefs.SetInt("Level", 1);
        }

        return PlayerPrefs.GetInt("Level");
    }

    public void SpawnCoin()
    {

        if (coinRoot.transform.childCount >= coinMaxCount)
        {
            return;
        }

    //    if (coinSpawnCount > coinSpawnMaxCount)
    //        return;

        GameObject coin = Instantiate(coinPrefab);

        coin.transform.parent = coinRoot.transform;
 
        Vector3 pos = curStage.InitialPos[Random.Range(0, curStage.InitialPos.Length)].transform.position;
        

        Vector3 addPos = new Vector3(Random.Range(-1.0f, 1.0f), 0, Random.Range(-1.0f, 1.0f));
        addPos.Normalize();
        addPos *= Random.Range(curStage.coinSpawnRadius * 0.2f, curStage.coinSpawnRadius);

        Vector3 newPos = pos + addPos + new Vector3(0, 0.0f, 0);


        RaycastHit hit;
        Ray ray = new Ray();
        ray.origin = newPos + new Vector3(0,5,0);
        ray.direction = new Vector3(0, -1, 0);

       // bool touched = false;
        
        if (Physics.Raycast(ray,out hit))
        {
            newPos = hit.point;
        }


        //if (Physics.Raycast(ray, out hit, LayerMask.NameToLayer("Ground")))
        //{
        //    if (hit.point.y > newPos.y)
        //        newPos = hit.point;
        //   // coin.transform.up = hit.normal;
        //}


        newPos += new Vector3(0, 0.75f, 0);

        coin.transform.position = newPos;

        if (!curStage.waterDead)
            coin.GetComponent<Coin>().isInWater = true;

        coinSpawnCount++;
    }
    

    // Update is called once per frame
    void Update()
    {
        bool isInRanking = false;

        int deadCount = 0;
        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].GetComponent<TubePlayer>().isDead)
                deadCount++;
        }

        if (deadCount >= players.Length - 3)
            isInRanking = true;

        currentCoinSpawnTime += Time.deltaTime;

        if (currentCoinSpawnTime > coinSpawnTime)
        {
            currentCoinSpawnTime -= coinSpawnTime;

            if (!isInRanking)
                SpawnCoin();
        }
        
        int aliveCount = 0;
        for (int i = 0;i < players.Length;i++)
        {
            if (players[i].GetComponent<TubePlayer>().isDead == false)
                aliveCount++;
        }

        if (aliveCount > 0)
        {
          //  dangerUpdator.GameObjectCollections = new GameObject[aliveCount];

            int index = 0;

            for (int i = 0; i < players.Length; i++)
            {
                if (players[i].GetComponent<TubePlayer>().isDead == false)
                {
              //      dangerUpdator.GameObjectCollections[index] = players[i].GetComponent<TubePlayer>().playerDummy;
                    index++;
                }
            }
        }

    //    dangerEnvironment.UpdateLayerGameObjects();
    }

    private void FixedUpdate()
    {
      
    }



    public int stageIndex = -1;

    void InitStage()
    {
        if (stages.Length > 0)
        {
            stageIndex = Random.Range(0, stages.Length);

            if (isStageMode)
                stageIndex = (GetLevel() - 1) % stages.Length;

            if (useTestStage)
                stageIndex = testStage;

            for (int i = 0; i < stages.Length; i++)
            {
                if (i == stageIndex)
                {
                    stages[i].gameObject.SetActive(true);
                    //    curStage = stages[i];
                }
                else
                    stages[i].gameObject.SetActive(false);
            }

            for (int i = 0; i < stages.Length; i++)
            {
                if (stages[i].gameObject.activeInHierarchy)
                {
                    curStage = stages[i];
                }
            }
        }
       
        

        players = new GameObject[playerCount];

        int playerIndex = Random.Range(0, playerCount);


        for (int i = 0; i < playerCount; i++)
        {
            if (i != playerIndex)
            {
                GameObject newPlayer = Instantiate(playerPrefab.gameObject);
                newPlayer.GetComponent<TubePlayer>().isPlayer = false;
                players[i] = newPlayer;
                newPlayer.transform.parent = playerPrefab.transform.parent;
            }
        }

        players[playerIndex] = playerPrefab.gameObject;

        for (int i = 0; i < players.Length; i++)
        {
            RaycastHit hit;
            Ray ray = new Ray();
            ray.origin = curStage.InitialPos[i].transform.position + new Vector3(0,10,0);
            ray.direction = new Vector3(0, -1, 0);


            if (Physics.Raycast(ray, out hit))
            {
                curStage.InitialPos[i].transform.position = hit.point;
            }

            //if (Physics.Raycast(ray, out hit))
            //{
            //    if (hit.point.y > curStage.InitialPos[i].transform.position.y)
            //        curStage.InitialPos[i].transform.position = hit.point;
            //    // coin.transform.up = hit.normal;
            //}


            curStage.InitialPos[i].transform.position += new Vector3(0, 0.75f, 0);

            players[i].transform.position = curStage.InitialPos[i].transform.position;
            players[i].transform.rotation = Quaternion.Euler(new Vector3(0, Random.Range(0, 360.0f), 0));

            players[i].GetComponent<Rigidbody>().angularVelocity = new Vector3();
            players[i].GetComponent<Rigidbody>().velocity = new Vector3();

            players[i].GetComponent<TubePlayer>().others = new GameObject[players.Length - 1];
         //   players[i].GetComponent<TubePlayer>().waterDead = curStage.waterDead;

            int index = 0;
            for (int j = 0; j < players.Length; j++)
            {
                if (i != j)
                {
                    players[i].GetComponent<TubePlayer>().others[index] = players[j];
                    index++;
                }
            }
        }

        for (int i = 0; i < coinMaxCount;i++)
        {
            SpawnCoin();
        }
        
    }
}
