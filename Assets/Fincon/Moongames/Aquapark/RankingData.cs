﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankingData : MonoBehaviour
{
    public GameObject crownGold, crownSilver, crownBronze;
    public GameObject selected;
    public GameObject money;

    public Text nameText;
    public Text moneyText;
    public Text scoreText;

    public GameObject skel;
    public GameObject coin;
    public Text     bonusText;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateData(TubePlayer player, int ranking,bool isInRanking,bool isGameOver)
    {
        crownGold.SetActive(false);
        crownSilver.SetActive(false);
        crownBronze.SetActive(false);

        if (isGameOver)
        {
            coin.SetActive(true);
            skel.SetActive(false);

            player.finalRanking = ranking;
            switch (ranking)
            {
            case 1: 
                crownGold.SetActive(true);
                    bonusText.text = "+50";
                    player.bonusCoin = 50;
                break;
            case 2: 
                crownSilver.SetActive(true);
                    bonusText.text = "+20";
                    player.bonusCoin = 20;
                    break;
            case 3: 
                crownBronze.SetActive(true);
                    bonusText.text = "+10";
                    player.bonusCoin = 10;
                    break;
            }
        }
        else
        {
            bonusText.text = "";
            if (player.isDead)
                skel.SetActive(true);
            else
                skel.SetActive(false);

            //          nameText.text = player.nickname;
        }

        if (player.isPlayer && player.isDead)
        {
        //    coin.SetActive(true);
        }

        nameText.text = ranking.ToString() + "." + player.nickname;
        //nameText.text = player.nickname;

        selected.SetActive(player.isPlayer);
        //        moneyText;
          scoreText.text = player.score.ToString();
     //   scoreText.text = player.level.ToString();


    }
}
