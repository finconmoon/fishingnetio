﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TubeComparer : IComparer<TubePlayer>
{
    public int Compare(TubePlayer x, TubePlayer y)
    {
        //int scoreX = x.score;
        //int scoreY = y.score;
        //int scoreX = x.liveTime;
        //int scoreY = y.liveTime;
        int scoreX = x.liveTime + x.score;
        int scoreY = y.liveTime + y.score;



        if (scoreX > scoreY)
            return -1;
        else if (scoreX < scoreY)
            return 1;

        return 0;
        // your custom compare code here.
    }
}

public class RankingList : MonoBehaviour
{
    public RankingData rankingDataBase;

    GameObject[] rankingDatas;

    public Text messege;

    StageController stageController;
    UIManager uiManager;

    public GameObject[] rankingPositions;

    bool isGameOver = false;

    // Start is called before the first frame update
    void Start()
    {
        uiManager = FindObjectOfType<UIManager>();
        stageController = FindObjectOfType<StageController>();

        rankingDatas = new GameObject[stageController.playerCount];

        rankingDatas[0] = rankingDataBase.gameObject;
        for (int i = 1; i < stageController.playerCount ; i++)
        {
            GameObject newData = (GameObject)Instantiate<GameObject>(rankingDataBase.gameObject, transform);

            newData.transform.position = rankingDataBase.gameObject.transform.position;
            newData.transform.localPosition += new Vector3(0, i * -30, 0);

            rankingDatas[i] = newData;
        }

        //messege.gameObject.SetActive(false);
    }

    List<TubePlayer> tubeList = new List<TubePlayer>();
    TubeComparer tubeComparer = new TubeComparer();
    // Update is called once per frame
    int deadCount = 0;
    void FixedUpdate()
    {
        if (uiManager.gameState == UIManager.GameState.GAMEOVER 
        && !uiManager.endingScene.activeSelf
        && isGameOver)
        {
            for (int i = 0; i < 3; i++)
            {
                tubeList[i].transform.parent = uiManager.endingScene.transform;
                tubeList[i].GetComponent<Rigidbody>().velocity = new Vector3();
                tubeList[i].GetComponent<Rigidbody>().angularVelocity = new Vector3();
                if (!tubeList[i].isDead)
                {
                    tubeList[i].transform.position = rankingPositions[i].transform.position;
                }


            }
            uiManager.ClearStage();
            if (stageController.isStageMode)
            {
                messege.text = "LEVEL CLEAR!";

                if (stageController.playerPrefab.ranking > 3)
                {
                    messege.text = "TRY AGAIN";
                }
            }
            else
            {
                messege.text = "FINISHED";
            }

            messege.gameObject.SetActive(true);
        }

        if (!isGameOver && deadCount >= stageController.players.Length - 1)
        {
            isGameOver = true;
        }



        deadCount = 0;
        for (int i = 0; i < stageController.players.Length; i++)
        {
            if (stageController.players[i].GetComponent<TubePlayer>().isDead)
                deadCount++;
        }


        //if (!isGameOver)
        {
            tubeList.Clear();
            for (int i = 0; i < stageController.players.Length; i++)
            {
                tubeList.Add(stageController.players[i].GetComponent<TubePlayer>());
            }
            tubeList.Sort(tubeComparer);
        }

        bool isInRanking = false;

        if (deadCount >= stageController.players.Length - 3)
            isInRanking = true;

        if (stageController.playerPrefab.isDead && stageController.playerPrefab.ranking == 3 && isInRanking)
        {
            isGameOver = true;
        }

        int order = 1;
        int passCount = 0;

        for (int i = 0; i < tubeList.Count; i++)
        {
            if (isGameOver)
                tubeList[i].isGameOver = true;

         
            rankingDatas[i].GetComponent<RankingData>().UpdateData(tubeList[i], order, isInRanking, isGameOver);

            tubeList[i].ranking = i + 1;
            order++;
        }



       
    }
}
