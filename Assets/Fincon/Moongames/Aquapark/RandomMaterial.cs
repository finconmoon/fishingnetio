﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMaterial : MonoBehaviour
{
    public Material[] materials;
    // Start is called before the first frame update
    void Awake()
    {
        DoRandom();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void SetBase()
    { 
        if (GetComponent<MeshRenderer>())
        {
            GetComponent<MeshRenderer>().material = materials[0];
        }

        if (GetComponent<SkinnedMeshRenderer>())
        {
            GetComponent<SkinnedMeshRenderer>().material = materials[0];
        }
    }

    public void SetColor(int index)
    {
        if (GetComponent<MeshRenderer>())
        {
            GetComponent<MeshRenderer>().material = materials[index];
        }

        if (GetComponent<SkinnedMeshRenderer>())
        {
            GetComponent<SkinnedMeshRenderer>().material = materials[index];
        }
    }


    public void DoRandom()
    {

        Material mat = materials[Random.Range(0, materials.Length)];


        if (GetComponent<MeshRenderer>())
        {
            GetComponent<MeshRenderer>().material = mat;
        }

        if (GetComponent<SkinnedMeshRenderer>())
        {
            GetComponent<SkinnedMeshRenderer>().material = mat;
        }
    }
}
