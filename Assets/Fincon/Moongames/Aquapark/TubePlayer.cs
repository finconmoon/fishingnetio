﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TubePlayer : MonoBehaviour
{
    public float moveForce = 10.0f;
    public float airTorque = 1.0f;
    public float moveDist = 10.0f;

    float normalAngularDrag = 0.0f;
    public float airAngularDrag = 1.0f;

    public bool isPlayer = false;


    public GameObject[] others;

    GameObject followTarget;
    Vector3 followPos;
    Vector3 beginDirection;
    Vector3 beginPos;
    
    public float maxVelocity = 10.0f;
    public float rotSpeed = 10.0f;
    float findTime = 3.0f;
    public bool isDead = false;
    public ParticleSystem waterSpray;

    public AudioClip seBounce;
    public AudioClip seDead;
    public AudioClip seGirlDead;
    public AudioClip seFallWater;
    public AudioClip seLevelUp;
    public AudioClip seCoin;

    public AudioSource audioSlide;
    public AudioSource audioAir;

    public int maxLevel = 10;

    float slideVolume;
    float airVolume;

    [HideInInspector]
    public TubePlayer finalTouchPlayer;
    float finalTouchTime = -1000.0f;
    [HideInInspector]
    public int level = 1;
    public int ranking = 8;
    float originalMass = 0.0f;
    float originalScale = 0.0f;

    [HideInInspector]
    public bool waterDead = false;

    [HideInInspector]
    public string nickname = "Player";
    public GameObject nicknameText;
    public int score = 0;
    public int bonusCoin = 0;

    public int finalRanking = 999;
    public int liveTime = 0;

    // Start is called before the first frame update
    public StageController stageController;
    Quaternion nicknameRot;
    
    public bool isGameOver = false;
    public bool isInWater = false;

    public GameObject ScoreAni;
    

    public float playerCollisionFreezeTime = 0.05f;
    public float collisionFreezeTime = 0.2f;
    public float myCollisionFreezeTime = 0.2f;
    public float colliionTorqueDamping = 0.75f;

    public float freezeTimeIncrease = 0.015f;

    public float maxVelocityIncrease = 0.025f;

    public float levelUpScale = 0.03f;

    float currentFreezeTime = 1000.0f;

    public float collisionAddForce = 10.0f;
    public float collisionAddForceIncrease = 5.0f;

    public float collisionUpForce = 10.0f;
    public float collisionUpForceIncrease = 1.0f;


    public GameObject[] tubes;
    public int[]        tubePrices;

    [HideInInspector]
    public int colorIndex = 0;

    [HideInInspector]
    public int tubeIndex = 0;

    [HideInInspector]
    public int meshIndex = 0;

//    bool isBoost = false;
    float boostTime = 0.5f;

    [HideInInspector]
    public Vector3 MDForce;

    public DynamicJoystick joystick;

    private void Awake()
    {
        score = 1;
        if (PlayerPrefs.HasKey("NickName") == false)
        {
            nickname = "Player";
            nickname += Random.Range(0, 100000).ToString();

            PlayerPrefs.SetString("NickName", nickname);
        }

        if (PlayerPrefs.HasKey("Color") == false)
        {
            PlayerPrefs.SetInt("Color", 0);
        }

        if (PlayerPrefs.HasKey("Tube") == false)
        {
            PlayerPrefs.SetInt("Tube", 0);
        }

        if (PlayerPrefs.HasKey("Mesh") == false)
        {
            PlayerPrefs.SetInt("Mesh", 0);
        }

        
        beginDirection = new Vector3();

        beginDirection.x = Random.Range(-1, 1);
        beginDirection.z = Random.Range(-1, 1);
        beginDirection.Normalize();

       

        originalMass = GetComponent<Rigidbody>().mass;
        originalScale = transform.localScale.y;

        Time.timeScale = 0.0f;
        normalAngularDrag = GetComponent<Rigidbody>().angularDrag;
    }

    void Start()
    {
        FindTarget();

        for (int i = 0; i < tubes.Length; i++)
        {
         //   tubes[i].SetActive(false);
        }

        if (isPlayer)
        {
            waterSpray.gameObject.SetActive(true);

            gameObject.AddComponent<AudioSource>();
            gameObject.AddComponent<AudioListener>();
            nickname = PlayerPrefs.GetString("NickName");

            audioSlide.gameObject.SetActive(true);
            audioAir.gameObject.SetActive(true);

          //  UpdatePlayerShop();
          //  colorIndex = GetColor();
          //  tubeIndex = GetTube();
        }
        else
        {
            if (Random.Range(0, 2) == 0)
                meshIndex = 0;
            else
                meshIndex = 1;

   
            if (gameObject.GetComponent<AudioSource>())
                 gameObject.GetComponent<AudioSource>().enabled = false;

            if (gameObject.GetComponent<AudioListener>())
                 gameObject.GetComponent<AudioListener>().enabled = false;
            
            nickname = "NickName";// NameGenerator.GetNickname();

            if (Random.Range(0, 10) < 3)
                nickname += Random.Range(0, 10000).ToString();
;

            int randomvalue = Random.Range(0, 10);

            if (randomvalue < 1)
            {
            }

            if (Random.Range(0,10) < 3)
            {
        //        tubes[Random.Range(0,tubes.Length)].SetActive(true);
            }
            else
            {
           //     tubes[0].SetActive(true);
            }
        }

        nicknameText = Instantiate(nicknameText);
        nicknameText.GetComponent<TextMesh>().text = nickname;
        nicknameText.SetActive(true);
        nicknameRot = nicknameText.transform.rotation;

        beginPos = transform.position;

    } 

    public int GetTube()
    {
        return PlayerPrefs.GetInt("Tube");
    }

    public int GetColor()
    {
        return PlayerPrefs.GetInt("Color");
    }

    public int GetMeshIndex()
    {
        return PlayerPrefs.GetInt("Mesh");
    }

    public void SelectTube(int index)
    {
        PlayerPrefs.SetInt("Tube", index);
        tubeIndex = index;
    }

    public void  SelectColor(int index)
    {
        PlayerPrefs.SetInt("Color", index);
        colorIndex = index;
    }

    public void SelectMesh(int index)
    {
        PlayerPrefs.SetInt("Mesh", index);
        meshIndex = index;
    }

    public void UpdatePlayerShop()
    {
        for (int i = 0; i < tubes.Length; i++)
        {
            tubes[i].SetActive(false);
        }

        tubes[GetTube()].SetActive(true);
        
        colorIndex = GetColor();
        tubeIndex = GetTube();
        meshIndex = GetMeshIndex();
    }

    public void UpdatePlayerShopByIndex()
    {
        for (int i = 0; i < tubes.Length; i++)
        {
            tubes[i].SetActive(false);
        }

   //   tubes[tubeIndex].SetActive(true);
        
        
    }

    public void ReloadNickName()
    {
        nickname = PlayerPrefs.GetString("NickName");
        nicknameText.GetComponent<TextMesh>().text = nickname;
    }

    [HideInInspector]
    public Vector3 currentVelocity;
    // Update is called once per frame
    void FixedUpdate()
    {
        if (!isDead)
            liveTime++;

        boostTime -= Time.fixedDeltaTime;
        currentFreezeTime += Time.fixedDeltaTime;
        currentVelocity = GetComponent<Rigidbody>().velocity;
        nicknameText.transform.position = transform.position + new Vector3(0, 4, 0);

        
        nicknameText.GetComponent<TextMesh>().text = level.ToString() + "\n" + nickname;

        bool freeze = false;

        if (isPlayer )
        {
            if (currentFreezeTime < myCollisionFreezeTime * 0.5f)
                freeze = true;
        }
        else if (currentFreezeTime < myCollisionFreezeTime)
        {
            freeze = true;
        }

        
        Vector3 move = new Vector3();
     
        if (isPlayer && !isDead && joystick && !isGameOver && !freeze)
        {
            move.x = joystick.Direction.x;
            move.z = joystick.Direction.y;
            move.y = 0.0f;
            move.Normalize();
        }


        if (!isPlayer)
        {
            findTime -= Time.fixedDeltaTime;

            if (findTime < 0.0f || (followPos - transform.position).magnitude < 1.0f)
                FindTarget();

            

          
        }
        
        {
            if (transform.up.y > 0.5f)
            {
                Vector3 forward = transform.forward;
                forward.y = 0.0f;
                forward.Normalize();

                Quaternion rot = Quaternion.FromToRotation(forward, move);
                transform.rotation =
                    Quaternion.Lerp(transform.rotation, transform.rotation * rot, Time.fixedDeltaTime * rotSpeed);
            }
        }

        if (Vector3.Dot(transform.up, new Vector3(0, 1.0f, 0)) < -0.6f && !isDead && inEarthCount > 0)
        {
            Dead();
        }
        

        if (!isDead)
        {
            GetComponent<Rigidbody>().AddForce(move * moveForce);
          //  else
            //    GetComponent<Rigidbody>().(move * airTorque);
        }

        float curMaxVelocity = maxVelocity;
        if (boostTime > 0.0f)
        {
            curMaxVelocity += maxVelocity;
            GetComponent<Rigidbody>().AddForce(MDForce * 2.0f);
        }

        if (GetComponent<Rigidbody>().velocity.magnitude > curMaxVelocity)
        {
            GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity.normalized * curMaxVelocity;
        }

        Vector3 pos = transform.position;
        pos.y = beginPos.y;

        RaycastHit hit;
        Ray ray = new Ray();
        ray.origin = transform.position + new Vector3(0, 5, 0);
        ray.direction = new Vector3(0, -1, 0);

        //playerDummy.transform.position = transform.position;
        


        if (GetComponent<Rigidbody>().velocity.magnitude > 6.0f)
        {
            if (waterSpray.isStopped)
                waterSpray.Play();
        }
        else
        {
            if (waterSpray.isPlaying)
                waterSpray.Stop();
        }

        if (isPlayer)
        {
            float vol = GetComponent<Rigidbody>().velocity.magnitude / maxVelocity;
            vol = Mathf.Min(1.0f, vol);

            if (inEarthCount > 0)
            {
                if (Input.GetMouseButton(0))
                    slideVolume = Mathf.Lerp(slideVolume , vol, Time.fixedDeltaTime * 0.5f);
                else
                    slideVolume = Mathf.Lerp(slideVolume, 0.0f, Time.fixedDeltaTime * 2.0f);

                airVolume = Mathf.Lerp(airVolume, 0.0f, Time.fixedDeltaTime * 2.0f);
            }
            else
            {
                airVolume = Mathf.Lerp(airVolume, vol, Time.fixedDeltaTime * 0.5f);
                slideVolume = Mathf.Lerp(slideVolume, 0.0f, Time.fixedDeltaTime * 2.0f);
            }

            audioSlide.volume = slideVolume;
            audioAir.volume = airVolume;


            if (isInWater)
            {
               // GetComponent<Rigidbody>().AddForce(new Vector3(0,downForce,0));
            }
        }
    }

    void FindTarget()
    {
        if (others == null || others.Length == 0)
            return;

        float minLength = 100000000.0f;
        for (int i = 0; i < others.Length; i++)
        {
            float length = (others[i].transform.position - transform.position).magnitude;
            if (minLength > length && length > 10.0f)
            {
                minLength = length;
                followTarget = others[i];
            }
        }

        followTarget = others[Random.Range(0, others.Length)];
        followPos = followTarget.transform.position;

        //       followPos = transform.position;
        followPos = new Vector3(Random.Range(-15, 15), 0, Random.Range(-15, 15));
        findTime = Random.Range(3.0f, 8.0f);
    }

    Vector3 FollowDirection()
    {
        //FindTarget();

        Vector3 direction = new Vector3();
        if (followTarget)
        {
            direction = followTarget.transform.position - transform.position;
            direction = followPos - transform.position;
            direction.y = 0.0f;
            direction.Normalize();
        }

        return direction;
    }

    int inEarthCount = 0;


    private void OnCollisionEnter(Collision collision)
    {
        TubePlayer anotherTube = collision.gameObject.GetComponent<TubePlayer>();
        if (anotherTube && !anotherTube.isDead && !isDead)
        {
            ContactPoint contact = collision.contacts[0];
            Rigidbody otherRigidbody = collision.rigidbody;

            Vector3 normal = contact.normal;

            normal.y = 0.0f;
            normal.Normalize();
            Vector3 force = normal * (collisionAddForce + collisionAddForceIncrease * (float)anotherTube.level);

           
            force += new Vector3(0,1,0) * (collisionUpForce + collisionUpForceIncrease * (float)anotherTube.level);

            GetComponent<Rigidbody>().AddForce(force, ForceMode.Impulse);

            currentFreezeTime = 0.0f;
            myCollisionFreezeTime = anotherTube.collisionFreezeTime;
            GetComponent<Rigidbody>().angularVelocity = GetComponent<Rigidbody>().angularVelocity * colliionTorqueDamping;

            PlaySound(seBounce);

            finalTouchPlayer = anotherTube;
            finalTouchTime = Time.realtimeSinceStartup;
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("Obstacle"))
        {
            ContactPoint contact = collision.contacts[0];
            Rigidbody otherRigidbody = collision.rigidbody;

            Vector3 normal = contact.normal;

            normal.y = 0.0f;
            normal.Normalize();
            Vector3 force = normal * (collisionAddForce + collisionAddForceIncrease * (float)maxLevel);


            force += new Vector3(0, 1, 0) * (collisionUpForce + collisionUpForceIncrease * (float)maxLevel);

            GetComponent<Rigidbody>().AddForce(force, ForceMode.Impulse);

            currentFreezeTime = 0.0f;   
            myCollisionFreezeTime = collisionFreezeTime;
            GetComponent<Rigidbody>().angularVelocity = GetComponent<Rigidbody>().angularVelocity * colliionTorqueDamping;

            PlaySound(seBounce);
            finalTouchTime = Time.realtimeSinceStartup;
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {  
            inEarthCount++;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            inEarthCount--;
        }
    }

    public void Dead()
    {
        if (isDead)
            return;

      //  nicknameText.SetActive(false);
        GetComponent<Rigidbody>().mass = 1.0f;
        isDead = true;
        gameObject.layer = LayerMask.NameToLayer("DeadPlayer");

        if (meshIndex == 0)
            PlaySound(seGirlDead);
        else
           PlaySound(seDead);

        if (finalTouchPlayer && finalTouchTime + 5.0f > Time.realtimeSinceStartup)
        {
            finalTouchPlayer.LevelUp(3);
        }
    }

    public void LevelUp(int upLevel)
    {
        if (isDead)
            return;

        score += 1;

        if (level >= maxLevel)
        {
            PlaySound(seCoin);
            return;
        }
        level += upLevel;

        collisionFreezeTime += freezeTimeIncrease;
        maxVelocity += maxVelocityIncrease;


    // maxVelocity += 0.5f;
        float newScale = 1.0f + levelUpScale * (float)(level - 1);
        transform.localScale = new Vector3(originalScale * newScale, originalScale * newScale, originalScale * newScale);
    //    GetComponent<Rigidbody>().mass = originalMass * newScale;

        PlaySound(seLevelUp);

        if (isPlayer)
        {
            ScoreAni.GetComponent<Text>().text = "+" + upLevel.ToString();
            ScoreAni.SetActive(true);
            ScoreAni.GetComponent<Animation>().Stop();
            ScoreAni.GetComponent<Animation>().Play();
        }
    }

    public void PlaySound(AudioClip clip)
    {
        if (!isPlayer)
            return;

        GetComponent<AudioSource>().clip = clip;
        GetComponent<AudioSource>().Play();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (!isDead && other.GetComponent<Coin>())
        {
            other.gameObject.SetActive(false);
            other.gameObject.transform.parent = null;
            Destroy(other.gameObject);

            LevelUp(1);


          //  maxVelocity += maxVelocity;

            boostTime = 1.0f;
           //   score++;
           //  PlaySound(seCoin);
        }
    }

}
