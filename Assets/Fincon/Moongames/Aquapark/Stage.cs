﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stage : MonoBehaviour
{
    public GameObject[] InitialPos;
    public bool useMeshDeformerSpawn = false;
    public bool waterDead = false;
    public GameObject water;
    public bool useGroundPickingOnly = false;
    public bool useMeshDeformerForce = false;
    public float meshDeformerForce = 0.0f;
    public float coinSpawnRadius = 10.0f;
    public Vector3 globalForce = new Vector3();
    // Start is called before the first frame update
    void Start()
    {
        if (useMeshDeformerSpawn)
        {
    //        GetComponent<MeshDeformer>().GetPoint();
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

}
