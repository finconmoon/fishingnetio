﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public bool isInWater = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (isInWater)
        {
            RaycastHit hit;
            Ray ray = new Ray();
            ray.origin = transform.position + new Vector3(0,1, 0);
            ray.direction = new Vector3(0, -1, 0);


            if (Physics.Raycast(ray, out hit, LayerMask.NameToLayer("Water")))
            {
                transform.position = hit.point + new Vector3(0, 0.75f, 0);
            }
        }
    }
}
