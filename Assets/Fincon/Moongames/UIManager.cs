﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    public enum  GameState
    {
        READY = 0,
        WAITGAME,
        GAME,
        GAMEFAST,
        CONTINUE,
        FINALCRASH,
        ONEMOREBOMB,
        GAMEOVER,
        SHOP,
    };
        
    public GameState gameState = GameState.READY;

    public GameObject    mainUI;
    public GameObject   waitGameUI;
    public GameObject    gameUI;
    public GameObject    gameFastUI;
    public GameObject    pauseUI;
    public GameObject    continueUI;
    public GameObject    finalCrashUI;
    public GameObject    oneMoreBombUI;
    public GameObject    resultUI;

    public GameObject    shopUI;
    public GameObject   shopCamera;

    public GameObject freeCoinButton;

    public     Text    Score;
    public     Text    gem;
    public     Text    bestScore;
    public     Text    countTime;
    public  Image   countImage;
    public     Text    round;
    public  Slider   powerSlider;
    public Text     gainGem;

    public    CommonManager commonManager;

    public  Image  lifeImage;
    public  Image  bombTimeImage;

    public  float    stateTime = 0.0f;
    public GameObject    newRecord;
    public GameObject    rewardAdButton;
    public    Text        needCoin;

    public GameObject    rateDialog;

    public GameObject   camera; 
    Shop shop;

    public Button[] levelUpButtons;
    public Text[] levelUpLV;
    public Text[] levelUpPrice;

    public Text drawCount;

    public Text gameModeText;


    public GameObject farMode;
    public GameObject slapstickMode;

    public GameObject rankingList;
    public Animator[] resultAnimators;

    public bool isGameOver = false;
    bool isAllEnd = false;

    public float deadTime = 0.0f;

    public TubePlayer player;

    public GameObject openingScene;
    public GameObject gameScene;
    public GameObject endingScene;

    public InputField nickNameField;

    public GameObject nextButton;
    public GameObject collectUI;

    

    private void Awake()
    {
        openingScene.SetActive(true);
        gameScene.SetActive(false);
        endingScene.SetActive(false);

        Time.timeScale = 1.0f;


    }

    // Use this for initialization
    void Start () {
        
        shop = FindObjectOfType<Shop>();
        SetState (GameState.READY);
        commonManager = FindObjectOfType<CommonManager> ();
        commonManager.Init ();
        bestScore.text = commonManager.GetBestScore ().ToString();
        rankingList.SetActive(false);

        nickNameField.text = PlayerPrefs.GetString("NickName");
        
    }
    
    public void EndEditNickName()
    {
        PlayerPrefs.SetString("NickName",nickNameField.text);
        player.ReloadNickName();
    }

    float waitGameTime = 0.0f;
    public void BeginGame()
    {
        #if UNITY_IPHONE
                Handheld.SetActivityIndicatorStyle(UnityEngine.iOS.ActivityIndicatorStyle.White);
        #elif UNITY_ANDROID
                Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Small);
        #endif

        Handheld.StartActivityIndicator();

        waitGameTime = Random.Range(0.0f, 2.0f);

        SetState(GameState.WAITGAME);
    }

    // Update is called once per frame
    void Update () {
    
        commonManager.score = player.score;
        gem.text = shop.GetGold().ToString();
        switch (gameState) {
        case GameState.READY:
            if (EventSystem.current.currentSelectedGameObject == null
                && Input.GetMouseButtonDown (0))
            {
                //SetState (GameState.GAME);
            }
                Time.timeScale = 1.0f;
                break;
        case GameState.WAITGAME:

                if (stateTime > waitGameTime)
                {
                    SetState(GameState.GAME);
                }
                break;
        case GameState.GAME:

                Handheld.StopActivityIndicator();
                Time.timeScale = 1.0f;
                //SetState(GameState.GAMEOVER);
                Score.text = commonManager.score.ToString();// + "m";

                if (player.isDead || player.isGameOver)
                    deadTime += Time.deltaTime;

                if (deadTime > 0.5f )
                {
                    SetState(GameState.GAMEOVER);
                }
                
                break;

        case GameState.GAMEFAST:
            float    fastTime = 24.999f;
            fastTime = 6.0f;
            countImage.fillAmount = stateTime / fastTime;
            countTime.text = ((int)((fastTime - stateTime) / (fastTime * 0.333f)) + 1).ToString ();
            if (stateTime > fastTime) {
                SetState (GameState.GAME);
            }
            break;
        case GameState.CONTINUE:
                lifeImage.fillAmount = stateTime / Time.timeScale / 2.0f;

                if (stateTime / Time.timeScale > 2.0f) {

                    SetState (GameState.GAMEOVER);
            }
            break;
    
        case GameState.GAMEOVER:
                Time.timeScale = 1.0f;
                gainGem.text = (player.score + player.bonusCoin).ToString();
                if (!isGameOver && stateTime > 2.0f)
                {
                    //  MGAdmob.instance.ShowBanner ();
                    SelectUI(resultUI);
                    Score.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);

                   // gainGem.text = commonManager.GetTotalScore().ToString();
                   // shop.GainGold(commonManager.GetTotalScore());

                    if (commonManager.GameEnd())
                    {
                        newRecord.SetActive(true);
                        bestScore.text = commonManager.GetBestScore().ToString();
                    }

                    //   if (commonManager.CanGetRewardAD())
                    //       freeCoinButton.SetActive(true);
                    //   else
                    freeCoinButton.SetActive(false);

                    isGameOver = true;

                    for (int i = 0; i < resultAnimators.Length; i++)
                    {
                        resultAnimators[i].enabled = true;
//                        resultAnimations[i].Play(resultAnimations[i].clip);
                    }
                }

                if (isAllEnd)
                {

                }


                break;
        }


        stateTime += Time.deltaTime;
    }

    public void     SetState(GameState state )
    {
        switch (state) {
        case GameState.READY:
            Time.timeScale = 1.0f;
            SelectUI (mainUI);
            break;
        case GameState.WAITGAME:
            Time.timeScale = 1.0f;
            SelectUI(waitGameUI);
            break;
        case GameState.GAME:
            openingScene.SetActive(false);
            gameScene.SetActive(true);
            endingScene.SetActive(false);

            SelectUI (gameUI);
            Time.timeScale = 1.0f;
            rankingList.SetActive(true);
            break;
        case GameState.GAMEFAST:
            SelectUI (gameFastUI);
            Time.timeScale = 3.0f;
            break;
        case GameState.CONTINUE:
            Time.timeScale = 1.0f;
            SelectUI (continueUI);
            //rewardAdButton.SetActive (commonManager.CanGetRewardAD ());
         
            break;
        case GameState.FINALCRASH:
            Time.timeScale = 1.0f;
            SelectUI (finalCrashUI);
            break;
        case GameState.ONEMOREBOMB:
            Time.timeScale = 1.0f;
            SelectUI (oneMoreBombUI);
            break;
        case GameState.GAMEOVER:
        
                break;
            case GameState.SHOP:
                SelectUI(shopUI);
//                shopCamera.SetActive(true);
                break;
        }

        gameState = state;
        stateTime = 0.0f;
    }

    void    HideAllUI()
    {
        mainUI.SetActive (false);
        gameUI.SetActive (false);
        gameFastUI.SetActive (false);
        pauseUI.SetActive (false);
        resultUI.SetActive (false);
        continueUI.SetActive (false);
        finalCrashUI.SetActive (false);
        oneMoreBombUI.SetActive (false);
        waitGameUI.SetActive(false);
        shopUI.SetActive(false);
    }

    public void    SelectUI(GameObject ui)
    {
        HideAllUI ();
        ui.SetActive (true);
    }

    public    void    ContinueGame()
    {
        SetState (GameState.GAME);
    }

    public void ClearStage()
    {
        openingScene.SetActive(false);
        gameScene.SetActive(false);
        endingScene.SetActive(true);
        
    }

    public void ChangeGameMode(int index)
    {
        if (index == 1)
        {
            commonManager.ChangeGameMode(CommonManager.GameMode.FAR);
        }
        else if (index == 0)
        {
            commonManager.ChangeGameMode(CommonManager.GameMode.SLAPSTICK);
        }
    }
    
    public    void    PauseGame()
    {
        //if (gameState == GameState.GAME || gameState == GameState.FINALCRASH) {
        //    SelectUI (pauseUI);    
        //    //Time.timeScale = 0.0f;
        //}
    }

    public    void    ResumeGame()
    {
        //if (gameState == GameState.GAME)
        //    SelectUI (gameUI);
        //else if (gameState == GameState.FINALCRASH)
        //    SelectUI (finalCrashUI);
        
        //Time.timeScale = 1.0f;
    }

    public    void    Restart()
    {
        commonManager.Restart ();
//        SceneManager.LoadScene ("GameScene");
    }

    public    void ShowShop()
    {
        SetState(GameState.SHOP);
    }

    public    void HideShop()
    {
        SelectUI(mainUI);
        player.UpdatePlayerShop();
        //shopUI.SetActive (false);
        //Time.timeScale = 1.0f;
    }

    public void HideLevelButton()
    {
        SelectUI(mainUI);
        player.UpdatePlayerShop();
        //shopUI.SetActive (false);
        //Time.timeScale = 1.0f;
    }

    public void CollectCoin()
    {
        collectUI.SetActive(false);
        nextButton.SetActive(true);
        int coin = player.score + player.bonusCoin;

        shop.GainGold(coin);
        //collectCoinText;
    }
}
