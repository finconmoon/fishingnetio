﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Advertisements;
using UnityEngine.SocialPlatforms.GameCenter;
//using Sdkbox;
using GoogleMobileAds;
using GoogleMobileAds.Api;

public class CommonManager : MonoBehaviour
{
    public enum GameMode
    {
        FAR,
        SLAPSTICK,
    };

    public static GameMode gameMode = GameMode.SLAPSTICK;

    string	LEADERBOARD_ID = "AFIORANKING";
    string  bestScoreName = "BestDistScore";

	public	int	score = 0; 
	Shop	shop;
	int		saveMeCount = 0;
	public int		saveMeGem = 1;

	static	CommonManager	commonManager;
	//public  Review review;

	UIManager	uiManager;
	Texture2D	capture;

    public Text        scoreText;
    public Text         scoreText2;

    public  int     comboScore = 0; 
	//AdColony.InterstitialAd _ad = null;

	static bool	initGPG = false;

    static  int fullBannerCount = 3;

    static int rewardCount = 7;

    public NewAdmob admob;
	void Awake() {

        admob = GetComponent<NewAdmob>();
        //PlayerPrefs.DeleteAll();

        scoreText.text = "";
        scoreText2.text = "";

		commonManager = this;
		shop = FindObjectOfType<Shop> ();
		uiManager = FindObjectOfType<UIManager> ();
		//review = FindObjectOfType<Review> ();

		if (PlayerPrefs.HasKey ("BestScore") == false)
			PlayerPrefs.SetInt("BestScore",0);

        if (PlayerPrefs.HasKey("BestDistScore") == false)
            PlayerPrefs.SetInt("BestDistScore", 0);
        
		capture = new Texture2D( Screen.width,Screen.height);


        ChangeGameMode(gameMode);

		#if UNITY_ANDROID
		LEADERBOARD_ID = "CgkIgqWdwogbEAIQAA";
		#endif

	}

	void Start () {

        /* Mandatory - set your AppsFlyer’s Developer key. */
      //  AppsFlyer.setAppsFlyerKey("zcKrZYJWnrWWctCxcLNnyT");
        /* For detailed logging */
        /* AppsFlyer.setIsDebug (true); */
#if UNITY_IOS
        /* Mandatory - set your apple app ID
         NOTE: You should enter the number only and not the "ID" prefix */
      //  AppsFlyer.setAppID("1474995128");
      //  AppsFlyer.trackAppLaunch();
#elif UNITY_ANDROID
  /* Mandatory - set your Android package name */
  //AppsFlyer.setAppID ("YOUR_ANDROID_PACKAGE_NAME_HERE");
  /* For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.*/
  //AppsFlyer.init ("YOUR_APPSFLYER_DEV_KEY","AppsFlyerTrackerCallbacks");
#endif
        
        Application.targetFrameRate = 60;

        //if (UnityEngine.iOS.Device.generation <= UnityEngine.iOS.DeviceGeneration.iPhone7Plus)
        //{
        //    Application.targetFrameRate = 30;
        //}
        //else
            //Application.targetFrameRate = 60;



		#if UNITY_ANDROID
		if (initGPG == false)
//			PlayGamesPlatform.Activate ();
		initGPG = true;

		#endif 

		if (Social.localUser.authenticated == false)
		{
			Social.localUser.Authenticate(success => {
				if (success)
				{
					string userInfo = "Username: " + Social.localUser.userName +
						"\nUser ID: " + Social.localUser.id +
						"\nIsUnderage: " + Social.localUser.underage;
					Debug.Log(userInfo);
				}
			});
		}
	}

	public void	Init()
	{
		score = 0; 
		saveMeCount = 0;
		saveMeGem = 1;
	}
		

    public void ChangeGameMode(GameMode mode)
    {
        gameMode = mode; 

        switch(gameMode)
        {
            case    GameMode.FAR: 
                LEADERBOARD_ID = "AFIORANKING";
            bestScoreName = "BestDistScore";
            break;
        case    GameMode.SLAPSTICK: 
            LEADERBOARD_ID = "AFIORANKING";
            bestScoreName = "BestScore";
            break;
        }
        Social.LoadScores(LEADERBOARD_ID, scores => {
        });
    }


    public int  GetTotalScore()
    {
        return score;
        
    }

    public void ScoreUp(string skillType,int    gainScore = 1)
    {
       // return;
        GetComponent<AudioSource>().pitch = 1.0f + (float)comboScore * 0.015f;
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().Play();

        if (gameMode == CommonManager.GameMode.SLAPSTICK)
        {
            comboScore += 1;

            int newScore = gainScore + comboScore - 1;
            score += newScore;

            scoreText2.text = skillType + "+" + (newScore).ToString();
            scoreText2.GetComponent<Animation>().Stop();
            scoreText2.GetComponent<Animation>().Play();
        }
        else
        {
            comboScore += gainScore;

            if (comboScore > 15)
                comboScore = 15;
            
            scoreText.text = skillType;
            scoreText.GetComponent<Animation>().Stop();
            scoreText.GetComponent<Animation>().Play();
        }



     //   ComboSpeedUp();
    }

	void OnDestroy()
	{
		//RewardBasedVideoAd.Instance.OnAdClosed -= OnAdmobRewardClosing;
	//	RewardBasedVideoAd.Instance.OnAdClosed -= OnAdmobOneMoreBomb;
		//RewardBasedVideoAd.Instance.OnAdClosed -= OnAdmobSaveMe;
	}

	// Update is called once per frame
	void Update () {


		if (Input.GetKeyDown(KeyCode.Space))
        {
			if (Time.timeScale == 1.0f)
				Time.timeScale = 0.0f;
			else
				Time.timeScale = 1.0f;
		}

		if (Input.GetKeyDown(KeyCode.Return))
		{
			ScreenCapture.CaptureScreenshot("ScreenShot" + Time.timeSinceLevelLoad.ToString() +".png");
		}


	}

	void OnApplicationPause(bool	_pause){
		if (_pause == true) {
			uiManager.PauseGame ();
		}
	}

	public int	GetBestScore()
	{
        return PlayerPrefs.GetInt (bestScoreName);
	}


    static float interstitialTime = -1000.0f;
	public	bool	GameEnd()
	{
        fullBannerCount--;
        //review.UserDidSignificantEvent (true);
        
        if (PlayerPrefs.HasKey("RateCount") == false)
            PlayerPrefs.SetInt("RateCount", 4);

        PlayerPrefs.SetInt("RateCount", PlayerPrefs.GetInt("RateCount") - 1);
        PlayerPrefs.Save();

        if (PlayerPrefs.GetInt("RateCount") == 0)
        {
            RateGame();

            if (PlayerPrefs.HasKey("RateMax") == false)
                PlayerPrefs.SetInt("RateMax", 10);

            PlayerPrefs.SetInt("RateCount", PlayerPrefs.GetInt("RateMax"));
            PlayerPrefs.SetInt("RateMax", PlayerPrefs.GetInt("RateMax") + 5);
            PlayerPrefs.Save();
        }
        else if (interstitialTime + 60.0f < Time.realtimeSinceStartup)
        {
            //if (Chartboost.hasInterstitial(CBLocation.Default) == true)
            //{
            //    Chartboost.showInterstitial(CBLocation.Default);
            //    interstitialTime = Time.realtimeSinceStartup;
            //}
            if (admob.IsInterstitialReady())
            {
                admob.ShowInterstitial();
                interstitialTime = Time.realtimeSinceStartup;
            }
            else if (Advertisement.IsReady("video"))
            {
                Advertisement.Show("video");
                interstitialTime = Time.realtimeSinceStartup;
            }
        }


        if (GetTotalScore() > PlayerPrefs.GetInt(bestScoreName)) {
            PlayerPrefs.SetInt (bestScoreName, GetTotalScore());
		
            Social.ReportScore(GetTotalScore(), LEADERBOARD_ID, success => {
				Debug.Log(success ? "Reported score successfully" : "Failed to report score");
			});

			return true;
		}


		return false;
	}

	public	void	Restart()
	{
		//Time.timeScale = 1.0f;
		Application.LoadLevel(Application.loadedLevel);
	}

	public	void	ShowScoreboard()
	{
        #if UNITY_IPHONE
       // Social.ShowLeaderboardUI();
        GameCenterPlatform.ShowLeaderboardUI (LEADERBOARD_ID, UnityEngine.SocialPlatforms.TimeScope.AllTime);
		#else
		//Social.ShowLeaderboardUI();
//		((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(LEADERBOARD_ID);
		#endif
	}

	public	void	RateGame()
	{
        UnityEngine.iOS.Device.RequestStoreReview();

	}

	public	void	RateNo()
	{
		uiManager.rateDialog.SetActive (false);
		PlayerPrefs.SetInt ("RateCount", -1);
		PlayerPrefs.Save ();
	}


	public	void	RateLater()
	{
		uiManager.rateDialog.SetActive (false);
		PlayerPrefs.SetInt ("RateCount", 8);
		PlayerPrefs.Save ();

	}


	public	void	LikeGame()
	{
        UnityEngine.iOS.Device.RequestStoreReview();
        //Application.OpenURL ("fb://profile/125675001388821");
    }

	byte[] imgBytes;
	uint imgLength;
	public GameObject screenshotView;


	public	void	ShareGame()
	{
        UnityEngine.iOS.Device.RequestStoreReview();
    }

	public bool	CanGetRewardAD(){

        rewardCount++;

        if (rewardCount < 12)
        {
            return false;
        }
        
        if (Advertisement.IsReady("rewardedVideo"))
        {
            return true;
        }
        else if (admob.IsRewardVideoReady())
        {
            return true;
        }

        return false;
	}

	public	bool	IsContinueEnable(){
		if (saveMeGem != -1)
			return true;

		return false;
	}
    
		
		

}
