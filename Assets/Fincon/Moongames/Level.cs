﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour {

	public	float	minWidth = 1.0f;
	public	float	maxWidth = 1.0f;
	public	float	minHeight = 1.0f;
	public	float	maxHeight = 1.0f;

	public	float	widthDiscount = 1.0f;
	public	float	heightDiscount = 1.0f;
	public	float	massDiscount = 1.0f;

	public	float	mass = 1.0f;
	public	float	unit = 1.0f;

	///public	float	fixedJointForce = 5.0f;
	public 	Vector3[]	boxScales;

	public	float	minFloorWidth = 1.0f;
	public	float	maxFloorWidth = 1.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public GameObject InstantiateBox(GameObject box)
	{
		GameObject newBox =  Instantiate(box);

		Vector3 newScale = new Vector3 (
			Random.Range (minWidth,maxWidth),
			Random.Range (minHeight,maxHeight),
			Random.Range (minWidth,maxWidth));

		newScale.x = (int)(newScale.x / unit) * unit;
		newScale.y = (int)(newScale.y / unit) * unit;
		newScale.z = (int)(newScale.z / unit) * unit;
		newScale.z = 1.0f;

		newBox.transform.localScale = newScale;

		if (boxScales.Length > 0) {

			Vector3 scale = boxScales [Random.Range (0, boxScales.Length)];
			scale.z = 1.0f;
			newBox.transform.localScale = scale;
		}

		//newBox.GetComponent<Rigidbody> ().mass = mass;

		newBox.transform.position = new Vector3 (Random.Range (-0.7f, 0.7f), 0, 0);//Random.Range(-0.7f,0.7f));

		/*
		if (boxes.Count > 2) {
			((GameObject)boxes [boxes.Count - 3]).AddComponent<FixedJoint> ().connectedBody = 
				((GameObject)boxes [boxes.Count - 2]).GetComponent<Rigidbody> ();
			((GameObject)boxes [boxes.Count - 3]).GetComponent<FixedJoint> ().breakForce = 
				currentLevel.mass * currentLevel.fixedJointForce;
			((GameObject)boxes [boxes.Count - 3]).GetComponent<FixedJoint> ().breakTorque =
				currentLevel.mass * currentLevel.fixedJointForce;
			//((GameObject)boxes [boxes.Count - 2]).GetComponent<FixedJoint> ().enableCollision = true;
		}*/

		minWidth *= widthDiscount;
		maxWidth *= widthDiscount;

		minHeight *= heightDiscount;
		maxHeight *= heightDiscount;

		//currentLevel.mass *= currentLevel.heightDiscount;
		mass *= massDiscount;


		return newBox;
	}

}
