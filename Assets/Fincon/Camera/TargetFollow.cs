﻿using UnityEngine;
using System.Collections;
public class TargetFollow : MonoBehaviour {


	public	GameObject	target;

	public	float	distance;
    public	Vector3	forwardVector;
    public Vector3 addPos;

	public	float	interpValue;
    public SmoothFollow smoothFollow;
    [HideInInspector]
    public bool isGameOver = false;
    Vector3 endPos;

    public bool ignoreX = false;
	// Use this for initialization
	void Start () {
	//	playerCar = FindObjectOfType<vehicleController> ().gameObject;
        transform.position = target.transform.position;

        //        target.GetComponent<vehicleController>().carFollow = this;

        smoothFollow.target = transform;
    }



	// Update is called once per frame
	//void LateUpdate () {
	void FixedUpdate () {
        UpdateFollow();
        
    }


    public	void	UpdateFollow()
	{

        Vector3 position = target.transform.position;

        //  Vector3 pos = target.transform.position + addPos;
        Vector3 pos = position + addPos + forwardVector.normalized * distance;
        //pos.y = 0.0f;


        if (ignoreX)
        {
            pos.x = 0.0f;
        }

        //        if (!target.GetComponent<vehicleController>().isFinish)
        transform.position = pos;
        //else
        //{
        //    if (!isGameOver)
        //    {
        //        isGameOver = true;
        //        endPos = pos;

        //        transform.position = endPos + new Vector3(0, 0, -endingDistance);
        //    }
        //}
    }
}
