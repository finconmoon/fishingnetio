﻿using UnityEngine;

//namespace UnityStandardAssets.Utility
//{
	public class SmoothFollow : MonoBehaviour
	{

		// The target we are following
		[SerializeField]
		public Transform target;
		// The distance in the x-z plane to the target
		[SerializeField]
        public float distance = 10.0f;
		// the height we want the camera to be above the target
		[SerializeField]
        public float height = 5.0f;

        [SerializeField]
        public float distanceDamping = 10000.0f;

        [SerializeField]
        public float rotationDamping;
		[SerializeField]
        public float heightDamping;

        public bool noDamping = false;

        public float shakeDuration = -1f;

        // Amplitude of the shake. A larger value shakes the camera harder.
        public float shakeAmount = 0.7f;
        public float decreaseFactor = 1.0f;

        // Use this for initialization
        void Start() {
                currentDist = distance;
             //   if ((Screen.height / Screen.width) > 16.0f / 9.0f)
             //      GetComponent<Camera>().fieldOfView *= (Screen.height / Screen.width) / (16.0f / 9.0f) ;
        }


        float currentDist; 
		// Update is called once per frame

		public	void	UpdateFollow()
		{
			// Early out if we don't have a target
			if (!target)
				return;
			
			// Calculate the current rotation angles
			var wantedRotationAngle = target.eulerAngles.y;
			var wantedHeight = target.position.y + height;
			var wantedDist = distance;

            var currentRotationAngle = transform.eulerAngles.y;
			var currentHeight = transform.position.y;

            if (noDamping)
            {
                // Damp the rotation around the y-axis
                currentRotationAngle = wantedRotationAngle;

                // Damp the height
                currentHeight = wantedHeight;

                currentDist = wantedDist;
             }
            else
            {
            // Damp the rotation around the y-axis
                currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.fixedDeltaTime);

                // Damp the height
                currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.fixedDeltaTime);

                currentDist = Mathf.Lerp(currentDist, wantedDist, distanceDamping * Time.fixedDeltaTime);
            }

           // Convert the angle into a rotation
           var currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);
			
			// Set the position of the camera on the x-z plane to:
			// distance meters behind the target
			transform.position = target.position;


			transform.position -= currentRotation * Vector3.forward * currentDist;
			
			// Set the height of the camera
			transform.position = new Vector3(transform.position.x, currentHeight , transform.position.z);
       
            // Always look at the target
              transform.LookAt(target.transform.position );
		}


        void FixedUpdate()
        //void LateUpdate()
        {
            UpdateFollow();

            if (shakeDuration > 0)
            {
                transform.position += Random.insideUnitSphere * shakeAmount;
            }
            else
            {
               // shakeDuration = 0f;
            }

            shakeDuration -= Time.deltaTime * decreaseFactor;
         }

        public void Shake(float duration)
        {
            shakeDuration = duration;
        }
	}
//}